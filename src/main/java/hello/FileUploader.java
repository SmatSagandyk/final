package hello;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class FileUploader {

    private static String spamDIR = "/home/smat/Документы/FinalProject/spam/";
    private static String uploadDIR = "/home/smat/Документы/FinalProject/files/";

    private static ZoneId zoneID = ZoneId.of("Asia/Almaty");

    public static void upload(MultipartFile file) throws IOException {

        byte[] bytes = file.getBytes();


        Path path = (containsSpam(file)) ?
                Paths.get(spamDIR + file.getOriginalFilename())
                : Paths.get(uploadDIR + String.format("%s/%s", LocalDate.now(zoneID), file.getOriginalFilename()));


        Path requiredPackage = Paths.get(uploadDIR + LocalDate.now(zoneID));

        if (!Files.exists(requiredPackage)) {
            Files.createDirectories(requiredPackage);
        }

        Files.write(path, bytes);

    }

    public static boolean isTxt(MultipartFile file) {
        return file.getOriginalFilename().contains(".txt");
    }

    public static boolean containsSpam (MultipartFile file) throws IOException {

        InputStream inputStream = new BufferedInputStream(file.getInputStream());
        Scanner in = new Scanner(inputStream);


        while (in.hasNextLine()) {

            String temp = in.nextLine();

            if (temp.toLowerCase().contains("spam")) {
                return true;
            }
        }

        return false;
    }

    public static List<String> getFiles(String date) {

        List<String> files = new ArrayList<>();

        File folder = new File(uploadDIR + date);
        for (File file : folder.listFiles())
            files.add(file.getName());

        return files;
    }

    public static List<String> getSpamFiles() {

        List<String> files = new ArrayList<>();

        File folder = new File(spamDIR);
        for (File file : folder.listFiles())
            files.add(file.getName());

        return files;
    }

    public static void deleteSpam() {
        File folder = new File(spamDIR);
        for (File file : folder.listFiles())
            file.delete();
    }
}
