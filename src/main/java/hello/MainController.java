package hello;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping
public class MainController {

    @RequestMapping(value="/upload", method = RequestMethod.POST)
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file) {

        System.out.println(file.getOriginalFilename());
        try {

            if (!FileUploader.isTxt(file))
                return new ResponseEntity(HttpStatus.FORBIDDEN);

            FileUploader.upload(file);


        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @GetMapping(value="/spamfiles")
    public List<String> getSpam() {
        return FileUploader.getSpamFiles();
    }

    @GetMapping(value="/files")
    public List<String> getFiles(@RequestParam("date") String date) {

        return FileUploader.getFiles(date);
    }

    @GetMapping(value="/delete")
    public @ResponseBody String delete(){

        FileUploader.deleteSpam();

        return "OK";
    }
}

